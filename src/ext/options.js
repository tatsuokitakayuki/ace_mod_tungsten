"use strict";

require("./menu_tools/overlay_page");

var dom = require("../lib/dom");
var oop = require("../lib/oop");
var config = require("../config");
var EventEmitter = require("../lib/event_emitter").EventEmitter;
var buildDom = dom.buildDom;

var modelist = require("./modelist");
var themelist = require("./themelist");

var themes = { Bright: [], Dark: [] };
themelist.themes.forEach(function(x) {
    themes[x.isDark ? "Dark" : "Bright"].push({ caption: x.caption, value: x.theme });
});
themes.Bright.sort(function(a, b) { return a.caption.toLowerCase() > b.caption.toLowerCase() ? 1 : -1 });
themes.Dark.sort(function(a, b) { return a.caption.toLowerCase() > b.caption.toLowerCase() ? 1 : -1 });

var modes = modelist.modes.map(function(x){ 
    return { caption: x.caption, value: x.mode }; 
});


var optionGroups = {
    Editor: {
        Theme: {
            path: "theme",
            type: "select",
            items: themes
        },
        "Keybinding": {
            type: "buttonBar",
            path: "keyboardHandler",
            items: [
                { caption : "Ace", value : null },
                { caption : "Vim", value : "ace/keyboard/vim" },
                { caption : "Emacs", value : "ace/keyboard/emacs" },
                { caption : "Sublime", value : "ace/keyboard/sublime" },
                { caption : "VSCode", value : "ace/keyboard/vscode" }
            ]
        },
        "Font size": {
            path: "fontSize",
            type: "number"
        },
        "Font family": {
            path: "fontFamily",
            type: "textField"
        },
        "Cursor style": {
            path: "cursorStyle",
            items: [
               { caption : "Ace",    value : "ace" },
               { caption : "Slim",   value : "slim" },
               { caption : "Smooth", value : "smooth" },
               { caption : "Smooth And Slim", value : "smooth slim" },
               { caption : "Wide",   value : "wide" }
            ]
        },
        "Overscroll": {
            type: "buttonBar",
            path: "scrollPastEnd",
            items: [
                { caption : "None", value : 0 },
                { caption : "Half", value : 0.5 },
                { caption : "Full", value : 1 }
            ]
        },
        "Atomic soft tabs": {
            path: "navigateWithinSoftTabs"
        },
        "Enable behaviours": {
            path: "behavioursEnabled"
        },
        "Wrap with quotes": {
            path: "wrapBehavioursEnabled"
        },
        "Enable auto indent": {
            path: "enableAutoIndent"
        },
        "Full line selection": {
            type: "checkbox",
            values: "text|line",
            path: "selectionStyle"
        },
        "Highlight active line": {
            path: "highlightActiveLine"
        },
        "Show invisibles": {
            path: "showInvisibles"
        },
        "Show indent guides": {
            path: "displayIndentGuides"
        },
        "Highlight indent guides": {
            path: "highlightIndentGuides"
        },
        "Persistent HScrollbar": {
            path: "hScrollBarAlwaysVisible"
        },
        "Persistent VScrollbar": {
            path: "vScrollBarAlwaysVisible"
        },
        "Animate scrolling": {
            path: "animatedScroll"
        },
        "Show gutter": {
            path: "showGutter"
        },
        "Highlight gutter line": {
            path: "highlightGutterLine"
        },
        "Show line numbers": {
            path: "showLineNumbers"
        },
        "Fixed gutter width": {
            path: "fixedWidthGutter"
        },
        "Print margin": {
            type: "number",
            path: "printMarginColumn"
        },
        "Show print margin": {
            path: "showPrintMargin"
        },
        "Highlight selected word": {
            path: "highlightSelectedWord"
        },
        "Show fold widgets": {
            path: "showFoldWidgets"
        },
        "Fade fold widgets": {
            path: "fadeFoldWidgets"
        },
        "Use textarea for IME": {
            path: "useTextareaForIME"
        },
        "Merge undo deltas": {
            path: "mergeUndoDeltas",
            items: [
               { caption : "Always",  value : "always" },
               { caption : "Never",   value : "false" },
               { caption : "Timed",   value : "true" }
            ]
        },
        "Incremental Search": {
            path: "useIncrementalSearch"
        },
        "Copy without selection": {
            path: "copyWithEmptySelection"
        },
        "Auto scroll editor info view": {
            path: "autoScrollEditorIntoView"
        },
        "Enable multiselect": {
            path: "enableMultiselect"
        }
    },
    Session: {
        Mode: {
            path: "mode",
            type: "select",
            items: modes
        },
        "Soft wrap": {
            type: "buttonBar",
            path: "wrap",
            items: [
               { caption : "Off",  value : "off" },
               { caption : "View", value : "free" },
               { caption : "margin", value : "printMargin" },
               { caption : "40",   value : "40" }
            ]
        },
        "Folding": {
            path: "foldStyle",
            items: [
                { caption : "Manual", value : "manual" },
                { caption : "Mark begin", value : "markbegin" },
                { caption : "Mark begin and end", value : "markbeginend" }
            ]
        },
        "Soft tabs": {
            path: "useSoftTabs"
        },
        "Tab size": {
            path: "tabSize",
            type: "number"
        },
        "New line mode": {
            type: "buttonBar",
            path: "newLineMode",
            items: [
                { caption : "Auto", value : "auto" },
                { caption : "LF", value : "unix" },
                { caption : "CR+LF", value : "windows" }
            ]
        },
        "First line number": {
            type: "number",
            path: "firstLineNumber"
        },
        "Indented soft wrap": {
            path: "indentedSoftWrap"
        },
        "Relative Line Numbers": {
            path: "relativeLineNumbers"
        },
        "Read-only": {
            path: "readOnly"
        }
    },
    Extensions: {
        "Basic autocompletion": {
            path: "enableBasicAutocompletion"
        },
        "Live autocompletion": {
            path: "enableLiveAutocompletion"
        },
        "Snippets": {
            path: "enableSnippets"
        },
        "Code Lens": {
            path: "enableCodeLens"
        },
        "Emmet": {
            path: "enableEmmet"
        },
        "Elastic Tabstops": {
            path: "useElasticTabstops"
        },
        "Linking": {
            path: "enableLinking"
        }
    }
};

var OptionPanel = function(editor, element) {
    this.editor = editor;
    this.container = element || document.createElement("div");
    this.groups = [];
    this.options = {};
};

(function() {
    
    oop.implement(this, EventEmitter);
    
    this.add = function(config) {
        if (config.Editor)
            oop.mixin(optionGroups.Editor, config.Editor);
        if (config.Session)
            oop.mixin(optionGroups.Session, config.Session);
        if (config.Extensions)
            oop.mixin(optionGroups.Extensions, config.Extensions);
    };
    
    this.render = function() {
        this.container.innerHTML = "";
        var tdAttributes = {colspan: 2, style: "font-weight: bold;"};
        buildDom(["table", {role: "presentation", id: "controls"},
            ["tbody", null, [
                ["tr", null, ["td", tdAttributes, "Editor"]],
                this.renderOptionGroup(optionGroups.Editor),
                ["tr", null, ["td", tdAttributes, "Session"]],
                this.renderOptionGroup(optionGroups.Session),
                ["tr", null, ["td", tdAttributes, "Extensions"]],
                this.renderOptionGroup(optionGroups.Extensions),
            ]]
        ], this.container);
    };
    
    this.renderOptionGroup = function(group) {
        return Object.keys(group).map(function(key, i) {
            var item = group[key];
            if (!item.position)
                item.position = i / 10000;
            if (!item.label)
                item.label = key;
            return item;
        }).sort(function(a, b) {
            return a.position - b.position;
        }).map(function(item) {
            return this.renderOption(item.label, item);
        }, this);
    };
    
    this.renderOptionControl = function(key, option) {
        var self = this;
        if (Array.isArray(option)) {
            return option.map(function(x) {
                return self.renderOptionControl(key, x);
            });
        }
        var control;
        
        var value = self.getOption(option);
        
        if (option.values && option.type != "checkbox") {
            if (typeof option.values == "string")
                option.values = option.values.split("|");
            option.items = option.values.map(function(v) {
                return { value: v, name: v };
            });
        }
        
        if (option.type == "buttonBar") {
            control = ["div", {role: "group", "aria-labelledby": option.path + "-label"}, option.items.map(function(item) {
                return ["button", { 
                    value: item.value, 
                    ace_selected_button: value == item.value, 
                    'aria-pressed': value == item.value, 
                    onclick: function() {
                        self.setOption(option, item.value);
                        var nodes = this.parentNode.querySelectorAll("[ace_selected_button]");
                        for (var i = 0; i < nodes.length; i++) {
                            nodes[i].removeAttribute("ace_selected_button");
                            nodes[i].setAttribute("aria-pressed", false);
                        }
                        this.setAttribute("ace_selected_button", true);
                        this.setAttribute("aria-pressed", true);
                    } 
                }, item.desc || item.caption || item.name];
            })];
        } else if (option.type == "number") {
            control = ["input", {type: "number", value: value || option.defaultValue, style:"width:3em", oninput: function() {
                self.setOption(option, parseInt(this.value));
            }}];
            if (option.ariaLabel) {
                control[1]["aria-label"] = option.ariaLabel;
            } else {
                control[1].id = key;
            }
            if (option.defaults) {
                control = [control, option.defaults.map(function(item) {
                    return ["button", {onclick: function() {
                        var input = this.parentNode.firstChild;
                        input.value = item.value;
                        input.oninput();
                    }}, item.caption];
                })];
            }
        } else if (option.type == "textField") {
            control = ["input", {type: "text", value: value || option.defaultValue, oninput: function() {
                self.setOption(option, this.value);
            }}];
            if (option.ariaLabel) {
                control[1]["aria-label"] = option.ariaLabel;
            } else {
                control[1].id = key;
            }
            if (option.defaults) {
                control = [control, option.defaults.map(function(item) {
                    return ["button", {onclick: function() {
                        var input = this.parentNode.firstChild;
                        input.value = item.value;
                        input.oninput();
                    }}, item.caption];
                })];
            }
        } else if (option.items) {
            var buildItems = function(items) {
                return items.map(function(item) {
                    return ["option", { value: item.value || item.name }, item.desc || item.caption || item.name];
                });
            };
            
            var items = Array.isArray(option.items) 
                ? buildItems(option.items)
                : Object.keys(option.items).map(function(key) {
                    return ["optgroup", {"label": key}, buildItems(option.items[key])];
                });
            control = ["select", { id: key, value: value, onchange: function() {
                self.setOption(option, this.value);
            } }, items];
        } else {
            if (typeof option.values == "string")
                option.values = option.values.split("|");
            if (option.values) value = value == option.values[1];
            control = ["input", { type: "checkbox", id: key, checked: value || null, onchange: function() {
                var value = this.checked;
                if (option.values) value = option.values[value ? 1 : 0];
                self.setOption(option, value);
            }}];
            if (option.type == "checkedNumber") {
                control = [control, []];
            }
        }
        return control;
    };
    
    this.renderOption = function(key, option) {
        if (option.path && !option.onchange && !this.editor.$options[option.path])
            return;
        var path = Array.isArray(option) ? option[0].path : option.path;
        this.options[path] = option;
        var safeKey = "-" + path;
        var safeId = path + "-label";
        var control = this.renderOptionControl(safeKey, option);
        return ["tr", {class: "ace_optionsMenuEntry"}, ["td",
            ["label", {for: safeKey, id: safeId}, key]
        ], ["td", control]];
    };
    
    this.setOption = function(option, value) {
        if (typeof option == "string")
            option = this.options[option];
        if (value == "false") value = false;
        if (value == "true") value = true;
        if (value == "null") value = null;
        if (value == "undefined") value = undefined;
        if (typeof value == "string" && parseFloat(value).toString() == value)
            value = parseFloat(value);
        if (option.onchange)
            option.onchange(value);
        else if (option.path)
            this.editor.setOption(option.path, value);
        this._signal("setOption", {name: option.path, value: value});
    };
    
    this.getOption = function(option) {
        if (option.getValue)
            return option.getValue();
        return this.editor.getOption(option.path);
    };
    
}).call(OptionPanel.prototype);

exports.OptionPanel = OptionPanel;
